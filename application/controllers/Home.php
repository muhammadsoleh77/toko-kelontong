<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  // Halaman Homepage (Halaman Utama Web)
  public function index()
  {
    $data = array(
      'title' => 'Toko Kelontong - Toko Online',
      'isi'   => 'template/home/list'
    );
    $this->load->view('template/layout/wrapper', $data, FALSE);
  }

}
