<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  // Halaman Login
  public function index()
  {
    // Validasi
    $this->form_validation->set_rules('username', 'Harap input', 'required', array(
      'required' => '%s Username'
    ));
    $this->form_validation->set_rules('password', 'Harap input', 'required', array(
      'required' => '%s Password'
    ));

    if($this->form_validation->run()){
      $username   = $this->input->post('username');
      $password   = $this->input->post('password');
      // Proses ke simple login
      $this->simple_login->login($username, $password);
    }
    // End Validasi

    $data = array( 'title' => 'Login Administrator' );
    $this->load->view('login/list', $data, FALSE);
  }

  // Fungsi Logout
  public function logout()
  {
    // Ambil fungsi logout dari simple_login
    $this->simple_login->logout();
  }

}
