<p>
  <a href="<?php echo base_url('admin/user/tambah') ?>" class="btn btn-success btn-md">
    <i class="fa fa-plus"></i> Tambah
  </a>
</p>

<?php
  // Notifikasi
  if($this->session->flashdata('sukses'))
  {
    echo '<p class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</p>';
  }
?>

<table class="table table-bordered" id="example1">
  <thead>
    <tr>
      <th class="text-center">NO</th>
      <th class="text-center">NAMA</th>
      <th class="text-center">EMAIL</th>
      <th class="text-center">USERNAME</th>
      <th class="text-center">LEVEL</th>
      <th class="text-center">ACTION</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; foreach ($user as $user) { ?>
    <tr>
      <td class="text-center"><?php echo $no ?></td>
      <td class="text-center"><?php echo $user->nama ?></td>
      <td class="text-center"><?php echo $user->email ?></td>
      <td class="text-center"><?php echo $user->username ?></td>
      <td class="text-center"><?php echo $user->akses_level ?></td>
      <td class="text-center">
        <a href="<?php echo base_url('admin/user/edit/' . $user->id_user) ?>" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
        <a href="<?php echo base_url('admin/user/delete/' . $user->id_user) ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
