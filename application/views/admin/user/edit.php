<?php
// Notifikasi error
echo validation_errors('<div class="alert alert-warning">','</div>');

// Form Open
echo form_open(base_url('admin/user/edit'.$user->id_user), 'class="form-horizontal"');
?>

<div class="form-group">
  <label class="col-md-2 control-label">Nama Pengguna</label>
  <input class="form-control" type="text" name="nama" placeholder="Nama Pengguna" value="<?php echo set_value $user->nama ?>" required>
</div>
<div class="form-group">
  <label class="col-md-2 control-label">Email</label>
  <input class="form-control" type="email" name="email" placeholder="Email" value="<?php echo set_value $user->email ?>" required>
</div>
<div class="form-group">
  <label class="col-md-2 control-label">Username</label>
  <input class="form-control" type="text" name="username" placeholder="Username" value="<?php echo set_value $user->username ?>" readonly>
</div>
<div class="form-group">
  <label class="col-md-2 control-label">Password</label>
  <input class="form-control" type="password" name="password" placeholder="Password" value="<?php echo set_value $user->password ?>" required>
</div>
<div class="form-group">
  <label class="col-md-2 control-label">Akses Level</label>
  <select class="form-control" name="akses_level">
    <option value="Admin">Admin</option>
    <option value="User" <?php if($user->akses_level=="User") { echo "selected"; } ?>>User</option>
  </select>
</div>
<div class="form-group">
  <label class="col-md-2 control-label"></label>
  <div class="col-md-5">
    <button class="btn btn-success btn-md" type="submit" name="submit">
      <i class="fa fa-save"></i> Simpan
    </button>
    <button class="btn btn-info btn-md" type="reset" name="reset">
      <i class="fa fa-times"></i> Reset
    </button>
  </div>
</div>

<!-- Form Close -->
<?php echo form_close(); ?>
